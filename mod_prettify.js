/**
 * postcss-prettify
 * основано на https://github.com/codekirei/postcss-prettify
 */

function getDepth(node) {
    let depth = 0
    let parent = node.parent
    while (parent.type !== 'root') {
        depth += 1
        parent = parent.parent
    }
    return depth
}

function doubleSpace(node) {
    node.raws.before += '\n'
}

function defined() {
    for (var i = 0; i < arguments.length; i++) {
        if (arguments[i] !== undefined) return arguments[i];
    }
}

function indent(node, depth) {
    const indentStr = '  '.repeat(depth)
    return ar => ar.forEach(key => {
        node.raws[key] = defined(node.raws[key], '').trim().concat(`\n${indentStr}`)
    })
}

//===============================================

/**
 * Append space to colon if necessary. See at-rule-spacing-colon test case.
 */
const params = {
    match: /(\(.*)(:)([^\s])(.*\))/g,
    replace: '$1$2 $3$4',
}

function atrule(node) {
    const nodeDepth = getDepth(node)
    indent(node, nodeDepth)(['before', 'after'])
    node.raws.between = node.nodes ? ' ' : ''
    if (node.params) {
        node.raws.afterName = ' '
        node.params = node.params.replace(params.match, params.replace)
    }
    if (nodeDepth === 0) doubleSpace(node)
}


//===============================================

function comment(node) {
    if (getDepth(node) === 0) doubleSpace(node)
}

function decl(node) {
    indent(node, getDepth(node))(['before'])
    node.raws.between = ': '
}

function rule(node) {
    const nodeDepth = getDepth(node)
    indent(node, nodeDepth)(['before', 'after'])
    node.raws.between = ' '
    node.raws.semicolon = true
    if (node.selector.indexOf(', ') >= 0) {
        node.selector = node.selector.replace(/, /g, ',\n')
    }
    if (nodeDepth === 0) doubleSpace(node)
}

//===============================================


function format(node) {
    return { atrule, comment, decl, rule }[node.type](node)
}

export default function (css) {
    css.walk(format);
    if (css.first && css.first.raws) css.first.raws.before = '';
}





