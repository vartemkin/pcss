function nodeDescr(node) {
    let type = node.type;
    if (type == "atrule") {
        return node.name + ":" + node.params;
    }
    if (type == "rule") {
        return "sel:" + node.selector;
    }
    if (type == "decl") {
        return "decl:" + node.prop + ":" + node.value;
    }
    if (type == "comment") {
        return "- comment -";//:<<<"+node.text+">>>";
    }
    return "undefined_type";
}


function insertOrUpdateNode(papa, index, selectors, node) {

    let selector = selectors.shift();

    // ищем наверх по нодам такой селектор
    let found = papa.nodes.find((_node, _i) => {
        //_i <= index && _node.selector && console.log(_node.selector, _node.nodes.length, selector);
        return _i <= index && _node.selector && _node.selector.trim() == selector
    });


    if (found == undefined) {

        // если не нашли,

        if (selectors.length > 0) {

            // создаем ноду которую надо вставить
            let clonedRule = node.clone();
            clonedRule.selector = selector;

            // если промежуточная
            console.log("Не нашли, создали промежуточную", nodeDescr(clonedRule));
            clonedRule.removeAll();

            // если это промежуточная от корня документа - вставляем перед текущей позицией
            if (node.parent == papa) {
                papa.insertBefore(node, clonedRule);
            } else {
                papa.append(clonedRule);
            }

            insertOrUpdateNode(clonedRule, 100500, selectors, node);

        } else {

            let clonedRule = node;
            clonedRule.selector = selector;

            // если последняя
            console.log("Не нашли, создали завершающую", nodeDescr(clonedRule));
            papa.append(clonedRule);
        }


    } else {
        // если нашли

        if (selectors.length > 0) { // если это промежуточная

            console.log("Нашли выше, используем как промежуточную", nodeDescr(found));
            insertOrUpdateNode(found, 100500, selectors, node);

        } else {
            // если последняя

            console.log("Нашли выше, используем как последнюю", nodeDescr(found));
            if (found !== node) { // чтобы не копировать из самого в себя

                // нельзя через перебор, так как при append элемент пропадает и индексы сдивигаются
                let temp = node.nodes.map(_z => _z);
                temp.map(_z => {
                    found.append(_z);

                    // length показывает что после каждого found.append(_z) массив nodes уменьшается
                    // поэтому сначала все индексы объектов загоняются в temp
                    //console.log("-> " + nodeDescr(_z), node.nodes.length);
                });

                //node.remove(); // старая потом удалится как пустая
                console.log("Перемещаем свойства", );



            } else {
                console.log("Свойства не перемещаем так как это она и есть");
            }
        }
    }


    return false;
}


String.prototype.replaceAll = function(search, replace){
    return this.split(search).join(replace);
}


export default function (css) {

    // переводим в scss

    css.walkRules(rule => {
        let papa = rule.parent;
        let i = papa.nodes.indexOf(rule);
        if (i == -1) i = 100500;

        let selector = rule.selector;

        selector = selector.replaceAll(/\s*::\s*/," _t1_");

        if (selector.indexOf("(") == -1) {
            selector = selector.replaceAll(/\s*:\s*/," _t2_");
        }

        selector = selector.replaceAll(/\s*>\s*/," _t3_");

        // filter for @charset "UTF-8";*,:after,:before{box-sizing:border-box}
        if (selector.startsWith(" ")) {
            selector = selector.substr(1);
        }


        let selectors = selector.split(/\s+/g);

        insertOrUpdateNode(papa, i, selectors, rule);

    })

    // декодируем селекторы
    css.walkRules(rule => {
        let selector = rule.selector;
        selector = selector.replaceAll(/^_t1_/,"&::");
        selector = selector.replaceAll(/^_t2_/,"&:");
        selector = selector.replaceAll(/^_t3_/,"& > ");
        rule.selector = selector;
    })

    // удалить пустые правила
    css.walkRules(rule => {
        if (rule.nodes.length == 0) {
            rule.remove();
        }
    })

}
