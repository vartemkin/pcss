// ==UserScript==
// @name         observe
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @grant        none
// @include      http://*
// @include      https://*
// ==/UserScript==



window.foundedClasses = [];

window.selDump = function() {
    return "'"+Object.keys(window.foundedClasses).join("',\n'")+"'";
};


function handleElem(elem) {
    if (elem && elem.classList) {
        for (let i = 0; i < elem.classList.length; i++) {
            let className = elem.classList[i];
            if (className) window.foundedClasses["."+className] = 1;
            if (elem.id) window.foundedClasses["#"+elem.id] = 1;
            if (elem.tagName) window.foundedClasses[elem.tagName.toLowerCase()] = 1;
        }
    }
}

function handleElems(elems) {
    for (let i = 0; i < elems.length; i++) {
        handleElem(elems[i])
    }
}


// Конфигурация observer (за какими изменениями наблюдать)
const config = {
    attributes: true,
    childList: true,
    subtree: true
};

// Функция обратного вызова при срабатывании мутации
const callback = function (mutationsList, observer) {

    for (let mutation of mutationsList) {

        //console.log(mutation);
        if (mutation.type === 'childList') {

            //console.log('A child node has been added or removed.');
            handleElems(mutation.addedNodes)

        } else if (mutation.type === 'attributes' && mutation.attributeName == "class") {
            //console.log('The ' + mutation.attributeName + ' attribute was modified.');
            handleElem(mutation.target)
        }
    }
};

// Создаем экземпляр наблюдателя с указанной функцией обратного вызова
const observer = new MutationObserver(callback);

// Начинаем наблюдение за настроенными изменениями целевого элемента

setTimeout(function () {

    console.log("observe");

    let elems = document.getElementsByTagName("*");
    handleElems(elems);

    observer.observe(document.body, config);

    // Позже можно остановить наблюдение
    //observer.disconnect();

}, 5000);




