
// разбиваем склеенные через запятую селекторы

export default function (css) {

    css.walkRules(rule => {
        let selectors = rule.selector.split(",").map(sel => sel.trim());
        if (selectors.length > 1) {
            selectors.map(selector => {
                let clonedRule = rule.clone();
                clonedRule.selector = selector;
                rule.parent.insertBefore(rule, clonedRule);
            })
            rule.remove();
        }

    })
}



