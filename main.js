import mod_split_by_comma from "./mod_split_by_comma";
import mod_nested_selectors from "./mod_nested_selectors";
import mod_unprefixer from "./mod_unprefixer";
import mod_discard_duplicates from "./mod_discard_duplicates";
import mod_used_rules from "./mod_used_rules";
import mod_replace_urls from "./mod_replace_urls";
import mod_prettify from "./mod_prettify";
import mod_chrome_coverage from "./mod_chrome_coverage";
import mod_optimize_props from "./mod_optimize_props";

const postcss = require('postcss');
const fs = require('fs');


//mod_chrome_coverage("Coverage-20200303T124306.json", "br9kl", "pure2.css");



let content = fs.readFileSync("css_chrome.css", "utf-8");

content = content.replace(/\s*!important/g, "!important");

let css = postcss.parse(content);

for(let i = 0; i < 100; i++) console.log("\r\n");

mod_split_by_comma(css);
//mod_used_rules(css);

mod_unprefixer(css);

//mod_replace_urls(css);

mod_nested_selectors(css);

//mod_optimize_props(css);
mod_discard_duplicates(css);

mod_prettify(css);


let result = css.toResult().css;

result = result.replace(/\s*!important/g, " !important");

fs.writeFileSync("2.scss", result);






