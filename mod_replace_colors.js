function parseColor(str) {
    str = str.toLowerCase();
    let found;
    found = str.match(/^#([0-9a-f]{6})$/i);
    if (found && found[1]) {
        return [
            parseInt(found[1].substr(0, 2), 16),
            parseInt(found[1].substr(2, 2), 16),
            parseInt(found[1].substr(4, 2), 16)
        ];
    }

    found = str.match(/^#([0-9a-f]{3})$/i);
    if (found && found[1]) {
        return [
            parseInt(found[1].substr(0, 2), 16),
            parseInt(found[1].substr(2, 2), 16),
            parseInt(found[1].substr(4, 2), 16)
        ];
    }

    found = str.match(/^rgb\s*\(\s*(\d+)\s*,\s*(\d+)\s*,\s*(\d+)\s*\)$/i);
    if(found && found[1]) {
        return [found[1],found[2],found[3]];
    }
    return undefined;
}



export default function (css) {

    let colors = [
        {in: "#2b2b35", out: "#001760"},
        {in: "#484755", out: "#0839b5"},
        {in: "#4ec2e7", out: "#f7c0b2"},
        {in: "#00c2eb", out: "#f78568"},
        {in: "#39b3d7", out: "#e58a73"},
    ];

    // оставить только цвета
    css.walkDecls(decl => {
        if (
            decl.prop == "color" ||
            decl.prop == "background-color" ||
            decl.prop == "border" ||
            decl.prop == "border-top" ||
            decl.prop == "border-bottom" ||
            decl.prop == "border-left" ||
            decl.prop == "border-right"
        ) {

            for (let color of colors) {
                if (decl.value.indexOf(color.in) != -1) {
                    decl.value = decl.value.replace(color.in, color.out);
                    return;
                }
            }
        }

        decl.remove();
    })

    // удалить atrule
    css.walkAtRules(rule => {
        rule.remove()
    })

    // удалить пустые правила
    css.walkRules(rule => {
        if (rule.nodes.length == 0) {
            rule.remove();
        }
    })
}

