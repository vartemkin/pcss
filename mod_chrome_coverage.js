// https://www.tring-web-design.co.uk/2019/04/chrome-devtools-code-coverage-parser/

const fs = require("fs");

export default function mod_chrome_coverage(coverageFile, partOfUrl, resultFile)
{
    let json = fs.readFileSync(coverageFile);
    let files = JSON.parse(json);

    let result = "";
    for (let file of files) {
        if (file.url.indexOf(partOfUrl) != -1) {
            console.log(file.url);
            for (let range of file.ranges) {
                console.log(range.start + " - " + range.end);
                result += file.text.substring(range.start, range.end) + "\n";
            }
        }
    }

    fs.writeFileSync(resultFile, result);
}




